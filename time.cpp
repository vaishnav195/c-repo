#include<iostream>
#include<vector>

using namespace std;

class Time {
private:
	// data members 
	int hours; 
	int minutes; 
	double seconds;
 public:
	 // member functions 
	 Time(int HH, int MM, double SS){
		 hours = HH;
		 minutes = MM;
		 seconds = SS;
	 }

	 Time(int HH, int MM){
		 hours = HH;
		 minutes = MM;
		 seconds = 0;
	 }

	 Time(){
		 hours = 0;
		 minutes = 0;
		 seconds = 0;
	 }
	 
	 void set_hours(int HH){
		 if (HH < 0 || HH >24) return;
		 hours = HH;
	 }

	 void set_minutes(int MM){
		 if (MM < 0 || MM > 60) return;
		 minutes = MM;
	 }

	 void set_seconds(double SS){
		 if (SS < 0 || SS > 60) return;
		 seconds = SS;
	 }

	 int get_hours() // returns hour value
		 const{
		 return hours;
	 }

	 int get_minutes() // returns minutes value
		 const{
		 return minutes;
	 }

	 double get_seconds() // returns seconds value
		 const{
		 return seconds;
	 }

	 friend ostream &operator<<(ostream &output, Time &TT)
	 {
		 output << "[ " << TT.hours << ":" << TT.minutes << ":" << TT.seconds << " ]" << endl;
		 return output;
	 }
}; //  end of class Time // member function definitions
class TimeSpan {
private:
	// data members 
	int interval;
	double seconds;
public:
	// member functions 
	TimeSpan(){   // default constructor
		seconds = 0;
	}

	TimeSpan(double SS){  // constructor takes one 
		seconds = SS;
	}
		
	double get_hours() const{	// returns hours value
		return (seconds / 3600.0);
	}

	double get_minutes() const{	  // returns minutes value
		return (seconds / 60.0);
	}

	double get_seconds() const{		 // returns seconds value
		return seconds;
	}

	friend ostream &operator<<(ostream &output, TimeSpan &TT)
	{
		int HH, MM;
		double S = TT.seconds;
		HH = (static_cast<int>(S)) / 3600;
		MM = ((static_cast<int>(S)) % 3600) / 60;
		S = S - (HH * 3600) - (MM * 60);
		output << "[ " << HH << ":" << MM << ":" << S << " ]" << endl;
		return output;
	}
}; // end of class TimeSpan // member function definitions
void main()
{
	Time dt2(13, 30);
	cout << dt2;
	cout << dt2.get_hours() << endl;
	cout << dt2.get_minutes() << endl;
	cout << dt2.get_seconds() << endl;

	TimeSpan ts1(65.5);
	cout << ts1;
	cout << ts1.get_hours() << endl;
	cout << ts1.get_minutes() << endl;
	cout << ts1.get_seconds() << endl;
	
}

#include <iostream>

using namespace std;

int fibonacci(int term)
{
			if (term == 0)

			return 0;

			else if (term == 1)

		        return 1;

			else

			return fibonacci(term - 1) + fibonacci(term - 2);
};

int main()
{
	int x, term;
	cout << "enter the term";
	cin >> term;
	try{
		if (term < 0)
			throw exception();
		x = fibonacci(term);
		cout << "fibonacci(" << term << ")=>" << x << endl;
		system("pause");
		return 0;
	}
	catch (exception &err){
		cerr << "Negative terms are not accepted";
	}
}

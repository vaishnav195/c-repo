#include <iostream>
#include <vector>
#include<string>
#include<fstream>
#include<time.h>

using namespace std;

void save(vector< vector<int> > ticboard, int nplayers, int size, int winseq, 
	int pno, int entval, vector<int> &interval, vector<string> &input)
{
	ofstream fout("TicTacToe.txt");
	fout << nplayers << endl << size << endl << winseq << endl << pno << endl << entval << endl;
	for (int i = 0; i < (size*size); i++)
	{
	 fout << interval[i] << endl;
	}
	for (int i = 0; i < size; i++)
	{
	 for (int j = 0; j < size; j++)
	 {
	  fout << ticboard[i][j] << endl;
	 }
	}
	for (int i = 0; i < (size*size); i++)
	{
	 fout << input[i] << endl;
	}
}

bool straight(vector<int> samples, int winseq, int &wNo, int size)
{
	vector<string> testing(winseq);
	bool valid = true;
	for (int i = 0; i <= size - winseq; i++)
	{
	 int start = samples[i];
	  for (int j = i + 1; valid && j < i + winseq; j++)
	  {
	   if (start != samples[j])
	   {
		valid = false;
	   }
	 }
	  if (valid && start != -1)
	  {
		wNo = start;
		return true;
	  }
	  valid = true;
	}
	return false;
}
void checkingforEND(vector< vector<int> > ticboard, int size, int winseq, 
	bool &gameOver, int values, vector<char> players)
{
	vector<int> samples(size);
	int plyno;
	int count = 0;
	for (int i = 0; !gameOver && i < size; i++)
	{ 
	 // rows for the tictactoe
	 for (int j = 0; j < size; j++)
	 {
	  samples[j] = ticboard[i][j];
	 }
	 if (straight(samples, winseq, plyno, size))
	 {
	  cout << "The winner of this game is: " << players[plyno] << endl;
	  gameOver = true;
	 }
	}
	for (int i = 0; !gameOver && i < size; i++)
	{ 
	 //colums for the tictctoe
	  for (int j = 0; j < size; j++)
	  {
		samples[j] = ticboard[j][i];
	  }
	  if (straight(samples, winseq, plyno, size))
	  {
		cout << "The winner of this game is: " << players[plyno] << endl;
		gameOver = true;
	  }
	}
	for (int i = winseq - 1; !gameOver && i <size; i++)
	{
	 for (int j = i; j >= 0; j--)
	 {
		samples[count] = ticboard[count][j];
			count++;
	 }
	 if (straight(samples, winseq, plyno, count))
	 {
		cout << "The winner of this game is: " << players[plyno] << endl;
		gameOver = true;
	 }
	 count = 0;
	}
	for (int i = size - winseq; !gameOver && i > 0; i--)
	{
	 for (int j = i; j <size; j++)
	 {
		samples[count] = ticboard[j][size - 1 - count];
		 count++;
	 }
	 if (straight(samples, winseq, plyno, count))
	  {
		cout << "The winner of this game is: " << players[plyno] << endl;
		gameOver = true;
	  }
	  count = 0;
	}
	for (int i = size - winseq; !gameOver && i >= 0; i--)
	{
		for (int j = i; j <size; j++)
		{
			samples[count] = ticboard[j][count];
			count++;
		}
		if (straight(samples, winseq, plyno, count))
		{
			cout << "The winner of this game is:" << players[plyno] << endl;
			gameOver = true;
		}
		count = 0;
	}
	for (int i = winseq - 1; !gameOver && i <size - 1; i++)
	{
		for (int j = i; j >= 0; j--)
		{
			samples[count] = ticboard[j][size - 1 - count];
			count++;
		}
		if (straight(samples, winseq, plyno, count))
		{
			cout << "The winner of this game is: " << players[plyno] << endl;
			gameOver = true;
		}
		count = 0;
	}

	vector<int>sample1((winseq - 1)*(winseq - 1));
	for (int k = 0; k + (winseq - 2)<size; k++)
	{
		//rows for the tictactoe
		for (int j = 0; j + (winseq - 2)<size; j++)
		{ 
		//columns for the tictactoe
		 for (int i = 0; i<(winseq - 1); i++)
		 {
		  for (int l = 0; l < (winseq - 1); l++)
			{
			 sample1[count] = ticboard[i + k][l + j];
			 count++;
			}
		 }
		 if (straight(sample1, (winseq - 1)*(winseq - 1), plyno, count))
		 {
		  cout << "The winner of this game is:" << players[plyno] << endl;
		  gameOver = true;
		 }
		 count = 0;
		}
	}
	if (values == (size*size))
	{
		cout << "Game Over." << endl;
		gameOver = true;
	}
	if (gameOver)
	{
		remove("TicTacToe.txt");
	}
}
void displayBoard(vector< vector<int> > ticboard, int size, vector<char> players)
{
	cout << endl;
	cout << "   ";
	for (int i = 1; i <= size; i++)
	{
		cout << " " << i << "  ";
	}
	cout << endl << endl;
	for (int i = 0; i < size; i++)
	{
	 cout << i + 1 << "  ";
	 for (int j = 0; j < size; j++)
	 {
	  if (ticboard[i][j] != -1)
	  {
	   cout << " " << players[(ticboard[i][j])] << " ";
	  }
	  else
	  {
		cout << "   ";
	  }
	  if (!(j == size - 1))
		cout << "|";
	 }
	 if (!(i == size - 1))
	 {
	  cout << endl << "   ";
		for (int k = 1; k <= size; k++)
		{
		 cout << "---";
		 if (!(k == size))
			cout << "+";
		}
	 }
	  cout << endl;
	}
}
bool checkValidInput(string rows, string clmn, int &row, int &col)
{
	string nStr1 = "";
	string nStr2 = "";
	for (int i = 0; i < static_cast<int>(rows.length()); i++)
	{
		if (rows.at(i) != ' ')
		{
			nStr1.append(1, rows.at(i));
		}
	}
	for (int i = 0; i < static_cast<int>(clmn.length()); i++)
	{
		if (clmn.at(i) != ' ')
		{
			nStr2.append(1, clmn.at(i));
		}
	}
	for (int i = 0; i<static_cast<int>(nStr1.length()); i++)
	{
		if (nStr1.at(i)<'0' || nStr1.at(i)>'9')
		{
			cout << endl << "Invalid:(" << nStr1 << ") Enter valid numbers or 'y' to save and exit.";
			return false;
		}
	}
	for (int i = 0; i<static_cast<int>(nStr2.length()); i++)
	{
		if (nStr2.at(i)<'0' || nStr2.at(i)>'9')
		{
			cout << endl << "Invalid:(" << nStr2 << ") Enter valid numbers or 'y' to save and exit.";
			return false;
		}
	}
	row = atoi(nStr1.c_str());
	col = atoi(nStr2.c_str());         
	return true;
}

bool checking(string rows, string cols, vector< vector<int> > &ticboard, int &pCount, int size, 
	int &entered, vector<char> players, bool &validInput, vector<int> &interval, clock_t &startTime, vector<string> &inputOrder)
{
	validInput = false;
	int row, col;
	if (!checkValidInput(rows, cols, row, col))
	{
		return false;
	}
	if (row >size || row<1)
	{
		cout << endl << "Invalid row: (" << row << ") Enter values from 1 to " << size;
		return false;
	}
	if (col >size || col<1)
	{
		cout << endl << "Invalid column (" << col << ") Enter values from 1 to " << size;
		return false;
	}
	if (ticboard[row - 1][col - 1] != -1)
	{
		cout << endl << "Invalid location: (" << players[ticboard[row - 1][col - 1]] << ") Enter another value.";
		return false;
	}
	ticboard[row - 1][col - 1] = pCount;

	pCount++;
	interval[entered] = clock() - startTime;
	inputOrder[entered].append(to_string(row - 1));
	inputOrder[entered].append(",");
	inputOrder[entered].append(to_string(col - 1));
	entered++;
	validInput = true;
	return true;
}
void playGame(vector< vector<int> > &ticboard, int &nPlayers, int &size, int &winseq, int &pNo, int &entered,
	vector<char> &players, vector<int> &interval, bool isSaved, vector<string> &inputOrder)
{
	clock_t begin_time;

	bool  gameOver = false, validInput = true;
	if (!isSaved)
		displayBoard(ticboard, size, players);
	endOfProgram:
	string input, row, col;
	while (!gameOver)
	{
		do{
			cout << endl << endl << "player number:" << players[pNo] << "Enter Row Value:.\n Or To save and exit type 'y': ";
			if (validInput)
			{
				begin_time = clock();
			}
			getline(cin, row);
			if (row == "y" || row == "Y")
			{
				save(ticboard, nPlayers, size, winseq, pNo, entered, interval, inputOrder);
				gameOver = true;
				goto endOfProgram;
			}
			cout << endl << endl << "Player " << players[pNo] << "Enter Column Value.\n Or To save and exit type 'y': ";
			getline(cin, col);
			if (col == "y" || col == "Y")
			{
				save(ticboard, nPlayers, size, winseq, pNo, entered, interval, inputOrder);
				gameOver = true;
				goto endOfProgram;
			}
		} while (!checking(row, col, ticboard, pNo, size, entered, players, validInput, interval, begin_time, inputOrder));
		if (pNo == nPlayers)
		{
			pNo = 0;
		}
		displayBoard(ticboard, size, players);
		checkingforEND(ticboard, size, winseq, gameOver, entered, players);
	}
}
bool validateInput(string &input)
{
	string newStr = "";
	for (int i = 0; i < static_cast<int>(input.length()); i++)
	{
		if (input.at(i) != ' ')
		{
			newStr.append(1, input.at(i));
		}
	}
	for (int i = 0; i<static_cast<int>(newStr.length()); i++)
	{
		if (newStr.at(i)<'0' || newStr.at(i)>'9')
		{
			cout << "Invalid Input, Please enter only numbers." << endl;
			return false;
		}
	}
	input = newStr;
	return true;
}

int main()
{
	int nPlayers, size, winseq;
	string splayer, Bsize, Bwinseq;
	vector<char> players = 
	{ 
		'X', 'O',
		'A', 'B',
		'C', 'D', 
		'E', 'F', 
		'G', 'H',
		'I', 'J',
		'K', 'L',
		'M', 'N',
		'P', 'Q',
		'R', 'S',
		'T', 'U', 
		'V', 'W', 
		'Y', 'Z' 
	};

	bool WIN = true, savegame = false;
	int pNo = 0, entered = 0;
	string in;
	Enter:
	cout << "Resume a saved game? (y/n) :";
	getline(cin, in);
	if (in == "y" || in == "Y")
	{
		savegame = true;
		goto resuming;
	}
	else if (in != "n" && in != "N")
	{
		cout << "Invalid input, please enter again." << endl;
		goto Enter;
	}
	Enter1:
	cout << "Enter the number of players : ";

	getline(cin, splayer);
	if (validateInput(splayer))
		nPlayers = atoi(splayer.c_str());
	else
		goto Enter1;
	Enter2:
	cout << "Enter the size of the board : ";
	
	getline(cin, Bsize);
	if (validateInput(Bsize))
		size = atoi(Bsize.c_str());
	else
		goto Enter2;
	Enter3:
	cout << "Enter the win sequence : ";
	
	getline(cin, Bwinseq);
	if (validateInput(Bwinseq))
		winseq = atoi(Bwinseq.c_str());
	else
		goto Enter3;
	if ((size*size) / nPlayers >= winseq && winseq <= size)
	{
		WIN = true;
	}
	else
	{
		WIN = false;
		cout << endl << "winning Sequence :" << winseq;
		cout << endl << "BoardSize :" << size;
		cout << endl << "No of Players :" << nPlayers << endl;
		cout << "There is no winning posibility with the above value";
	}
	
	if (WIN)
	{
		vector< vector<int> > board(size, vector<int>(size, -1));
		vector<int> interval((size*size), -1);
		vector<string> inputOrder((size*size), "");
		playGame(board, nPlayers, size, winseq, pNo, entered, players, interval, false, inputOrder);
	}
	resuming:
	if (savegame)
	{
	 ifstream fin("TicTacToe.txt");
	  if (!fin.is_open())
	  {
		cout << "No saved game.";
		goto end;
	}
		fin >> nPlayers >> size >> winseq >> pNo >> entered;
		vector< vector<int> > board(size, vector<int>(size, -1));
		vector<int> interval1((size*size), -1);
		vector<string> inputOrder((size*size), "");
		for (int i = 0; i < (size*size); i++)
		{
			fin >> interval1[i];
		}
		for (int i = 0; i < size; i++)
		{
			for (int j = 0; j < size; j++)
			{
				fin >> board[i][j];
			}
		}
		for (int i = 0; i < (size*size); i++)
		{
			fin >> inputOrder[i];
		}
		{
			vector< vector<int> > tempBoard(size, vector<int>(size, -1));
			for (int i = 0; i < entered; i++)
			{
			 int pos = inputOrder[i].find(",");
			 int row = atoi(inputOrder[i].substr(0, pos).c_str());
			 int col = atoi(inputOrder[i].substr(pos + 1, static_cast<int>(inputOrder[i].length())).c_str());
			 tempBoard[row][col] = board[row][col];
			 clock_t begin_time = clock();
				while (clock() - begin_time < interval1[i])
				{

				}
				displayBoard(tempBoard, size, players);
			}
		}
		fin.close();
		playGame(board, nPlayers, size, winseq, pNo, entered, players, interval1, true, inputOrder);
	}
end:
	cout << endl << "End";
	char x;
	cin >> x;
	return 0;
}
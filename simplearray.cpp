#include <iostream>
#include <string>
#include <cstring>

using namespace std;

class my_int_array
{

private: int *pointer; 
		 int num;
	 
public: my_int_array(int size)
	{
		pointer = new int[size];
		num = size;
	}

	my_int_array(int *a, int num)
	{
		pointer = new int[num];
		pointer = a;
		memcpy(pointer, a, num);
	}

	my_int_array(const my_int_array &that)
	{
		*pointer = *that.pointer;
		num = that.num;
	}

	my_int_array& operator = (const my_int_array &that)
	{
		*(pointer) = *that.pointer;
		return *this;
	}

	int size()
	{
		return num;
	}

	int& at(int index)
	{
		if (index < num)
			return *(pointer + index);
		else
			cout << " index is out of bounds." << endl;
	}

	~my_int_array() 
	{
		if (pointer != nullptr)
			delete pointer;
	}
};

void main(){
	my_int_array arr(5);
	arr.at(2) = 10;
	int a = arr.at(2);
	cout << arr.at(2) << endl;
}

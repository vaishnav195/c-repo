#include<iostream>
#include<vector>

using namespace std;
vector< vector<char> >tab(3, vector<char>(4));//dynamic vector

//displaying the tic tac toe table.

void tictactoe()
{
	cout << "  " << "1" << "   " << "2" << "   " << "3" << endl;
	cout << "1 " << tab[0][0] << " | " << tab[0][1] << " | " << tab[0][2] << endl;
	cout << " ---+---+---" << endl;
	cout << "2 " << tab[1][0] << " | " << tab[1][1] << " | " << tab[1][2] << endl;
	cout << " ---+---+---" << endl;
	cout << "3 " << tab[2][0] << " | " << tab[2][1] << " | " << tab[2][2] << endl;
}

void main()
{

	int  r, c, count = 0;
	for (r = 0; r<3; r++)
		for (c = 0; c<3; c++)
			tab[r][c] = '\0';
				tictactoe();

	while (count<10)
	{
		//player 1 inputs
		cout << "symbol for player1 is X" << endl;
		cout << "enter the cell number you want to mark as X:" << endl;
		//row
		cin >> r;
		//coloumn
		cin >> c;

		//if condition if the inputs are  satisfying the conditions
		if (r<1 || r>3 || c<1 || c>3 || ('x' == tab[r - 1][c - 1] || 'o' == tab[r - 1][c - 1]))
		{
			cout << "the selected cell is not available" << endl;
			cout << "type again" << endl;
			cout << "enter the row value:";
			cin >> r;
			cout << "enter the coloumn value:";
			cin >> c;
		}
		tab[r - 1][c - 1] = 'x'; // this is to convert the real world inputs to vector accepted inputs
								// if user types 1,1; it will be converted into 0,0;
		count++;
		tictactoe();

		if (count == 9) //if condition for checking if its a draw
		{
			tictactoe();
			cout << "\n\t IT IS A DRAW";
			break;
		}

		//series of if condition for checking the WINNER

		if (tab[0][0] == 'x' && tab[0][1] == 'x' && tab[0][2] == 'x')
		{
			tictactoe();
			cout << "\n WINNER is PLAYER 1";
			cout << "\n GOOD GAME";
			break;
		}

		if (tab[1][0] == 'x' && tab[1][1] == 'x' && tab[1][2] == 'x')
		{
			tictactoe();
			cout << "\n WINNER is PLAYER 1";
			cout << "\n GOOD GAME";
			break;
		}

		if (tab[2][0] == 'x' && tab[2][1] == 'x' && tab[2][2] == 'x')
		{
			tictactoe();
			cout << "\n WINNER is PLAYER 1";
			cout << "\n GOOD GAME";
			break;
		}

		if (tab[0][0] == 'x' &&tab[1][0] == 'x' && tab[2][0] == 'x')
		{
			tictactoe();
			cout << "\n WINNER is PLAYER 1";
			cout << "\n GOOD GAME";
			break;
		}

		if (tab[0][1] == 'x' && tab[1][1] == 'x' && tab[2][1] == 'x')
		{
			tictactoe();
			cout << "\n WINNER is PLAYER 1";
			cout << "\n GOOD GAME";
			break;
		}

		if (tab[0][2] == 'x' && tab[1][2] == 'x' && tab[2][2] == 'x')
		{
			tictactoe();
			cout << "\n WINNER is PLAYER 1";
			cout << "\n GOOD GAME";
			break;
		}

		if (tab[0][0] == 'x' && tab[1][1] == 'x' && tab[2][2] == 'x')
		{
			tictactoe();
			cout << "\n WINNER is PLAYER 1";
			cout << "\n GOOD GAME";
			break;
		}

		if (tab[0][2] == 'x' && tab[1][1] == 'x' && tab[2][0] == 'x')
		{
			tictactoe();
			cout << "\n WINNER is PLAYER 1";
			cout << "\n GOOD GAME";
			break;
		}


		//player 2 inputs
		cout << "symbol for player2 is O" << endl;
		cout << "enter the cell number you want to mark as O:" << endl;
		//row
		cin >> r;
		//coloumn
		cin >> c;

		//if condition to check if the inputs are satisfying the conditions
		if (r<1 || r>3 || c<1 || c>3 || ('x' == tab[r - 1][c - 1] || 'o' == tab[r - 1][c - 1]))
		{
			cout << "Selected cell is not available";
			cout << "type again"<<endl;
			cout << "enter the row value:";
			cin >> r;
			cout << "enter the coloumn value:";
			cin >> c;
		}
		tab[r - 1][c - 1] = 'o';
		count++;
		tictactoe();

		//checking if the player wins or not
		if (tab[0][0] == 'o' && tab[0][1] == 'o' && tab[0][2] == 'o')
		{
			tictactoe();
			cout << "WINNER is PLAYER2" << endl;;
			cout << "GOOD GAME" << endl;
			break;
		}

		if (tab[1][0] == 'o' && tab[1][1] == 'o' && tab[1][2] == 'o')
		{
			tictactoe();
			cout << "WINNER is PLAYER2" << endl;;
			cout << "GOOD GAME" << endl;
			break;
		}

		if (tab[2][0] == 'o' && tab[2][1] == 'o' && tab[2][2] == 'o')
		{
			tictactoe();
			cout << "WINNER is PLAYER2" << endl;;
			cout << "GOOD GAME" << endl;
			break;
		}

		if (tab[0][0] == 'o' &&tab[1][0] == 'o' && tab[2][0] == 'o')
		{
			tictactoe();
			cout << "WINNER is PLAYER2" << endl;;
			cout << "GOOD GAME" << endl;
			break;
		}

		if (tab[0][1] == 'o' && tab[1][1] == 'o' && tab[2][1] == 'o')
		{
			tictactoe();
			cout << "WINNER is PLAYER2" << endl;;
			cout << "GOOD GAME" << endl;
			break;
		}

		if (tab[0][2] == 'o' && tab[1][2] == 'o' && tab[2][2] == 'o')
		{
			tictactoe();
			cout << "WINNER is PLAYER2" << endl;;
			cout << "GOOD GAME" << endl;

			break;
		}

		if (tab[0][0] == 'o' && tab[1][1] == 'o' && tab[2][2] == 'o')
		{
			tictactoe();
			cout << "WINNER is PLAYER2" << endl;;
			cout << "GOOD GAME" << endl;
			break;
		}

		if (tab[0][2] == 'o' && tab[1][1] == 'o' && tab[2][0] == 'o')
		{
			tictactoe();
			cout << "WINNER is PLAYER2" << endl;;
			cout << "GOOD GAME"<<endl; 
			break;
		}
		
		


	}

		cout << "Thank you for Playing"<<endl;
		system("pause");
	}



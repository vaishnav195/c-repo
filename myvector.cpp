#include<iostream>
#include<string>

using namespace std;

template<class T>

class myvector
{
public:
		T *ptr;
		int v_si; 
		int cap;
		
public:
		myvector()
		{
			v_si = 0;
			cap = 0;
			ptr = new T[];
		}
		myvector(int const si)
		{
			v_si = si;
			cap = si;
			ptr = new T[si];
			for (int i = 0; i < v_si; i++)
			{
				ptr[i] = NULL;
			}
		}
		myvector(const myvector &inp)
		{
			v_si = inp.v_si;
			cap = v_si + 6;
			ptr = new T[cap];
			for (int i = 0; i != v_si; i++)
			{
				ptr[i] = inp.ptr[i];
			}
		}
		myvector& operator=(const myvector &vector)
		{
			v_si = vector.v_si;
			cap = vector.cap;
			ptr = new T[v_si];
			delete[] ptr;
			for (int i = 0; i < v_si; i++)
			{
				ptr[i] = vector.ptr[i];
			}
			return *this;
		}
		myvector& operator=(myvector const &&inp)
		{
			v_si = inp.si();
			cap = inp.capa();
			ptr = inp.ptr;
			return *this;
		}
		myvector(myvector &&inp)
		{
			v_si = inp.si();
			cap = inp.capa();
			ptr = inp.ptr;
			inp.ptr = nullptr;
		}
		myvector(T inp2[], int si)
		{
			v_si = si;
			cap = si;
			ptr = new T[si];
			for (int i = 0; i < si; i++)
			{
				ptr[i] = other[i];
			}
		}
		int si() const // return the current length of the vector
		{
			return v_si;
		}
		int capa() const // return the current capacity of the vector
		{
			return cap;
		}
		bool empty() // return whether or not the vector is empty
		{
			int e = 0;
			for (int y = 0; y < v_si; y++)
			{
				if (ptr[y] != NULL)
				{
					e++;
				}
			}
			if (e == 0)
			{
				return true;
			}
			else{
					return false;
				}
		}

		void resize(int resize) // resize the vector (change length) to the specified size, changing capacity as needed
		{
			if (resize >= 0)
			{
				if (cap < resize)
				{
					T *temp;
					temp = new T[v_si];
					for (int i = 0; i < v_si; i++)
					{
						temp[i] = ptr[i];
					}
					delete[] ptr;
					ptr = new T[resize];
					for (int i = 0; i < v_si; i++)
					{
						ptr[i] = temp[i];
					}
					for (int i = v_si; i<(resize - v_si); i++)
					{
						ptr[i] = NULL;
					}
					v_si = resize;
					cap = resize;
					delete[] temp;
				}
				else if (cap >= resize)
				{
					if (v_si>resize)
					{
						for (int i = v_si - 1; i >= resize; i--)
						ptr[i] = NULL;
						v_si = resize;
					}
					else if (v_si < resize)
					{
						for (int i = v_si; i < resize; i++)
						ptr[i] = NULL;
						v_si = resize;
					}
				}
		}
	}
	void reserve(int reserve) // change the vector capacity to the specified size, or give an error if capacity would get smaller than length
	{
		if (cap < reserve)
		{
			T *temp;
			temp = new T[v_si];
			for (int i = 0; i<v_si; i++)
			{
				temp[i] = ptr[i];
			}
			delete[] ptr;
			ptr = new T[reserve];
			for (int i = 0; i<v_si; i++)
			{
				ptr[i] = temp[i];
			}
			for (int i = v_si; i<reserve; i++)
			{
				ptr[i] = NULL;
			}
			v_si = reserve;
			cap = reserve;
			delete[] temp;
		}
		else if (cap>reserve)
		{
			cout << "length should be greater than cap";
		}
	}

	void assign(int ind, T const &index) // assign the second argument as the value of the vector at the position of the first argument
	{
		if (ind < cap && ind >= 0)
		{
			ptr[ind] = index;
		}
		else if (ind > cap)
		{
			cout<<"Inputed index cannot be out of range";
		}
	}
	void push_back(T const & element) // increase length by 1 and add the argument as the value at the end
	{
		if (cap < v_si + 1)
		{
			T *temp;
			temp = new T[v_si + 1];
			for (int i = 0; i < v_si; i++)
			{
				temp[i] = ptr[i];
			}
			temp[v_si] = element;
			delete[] ptr;
			ptr = new T[v_si + 1];
			for (int i = 0; i <= v_si; i++)
			{
				ptr[i] = temp[i];
			}
			cap = cap + 1;
			v_si +=1;
			delete[] temp;
		}
		else if (cap >= v_si + 1)
		{
			T *temp;
			temp = new T[v_si + 1];
			for (int i = 0; i < v_si; i++)
			{
				temp[i] = ptr[i];
			}
			temp[v_si] = element;
			delete[] ptr;
			ptr = new T[v_si + 1];
			for (int i = 0; i <= v_si; i++)
			{
				ptr[i] = temp[i];
			}
			v_si += 1;
			delete[] temp;
		}
	}
	T pop_back() // decrease length by 1, returning the value removed from the vector
	{
		if (v_si > 0)
		{
			T x = ptr[v_si - 1];
			T *temp;
			temp = new T[v_si - 1];
			for (int i = 0; i < v_si - 1; i++)
				temp[i] = ptr[i];
				delete[] ptr;
				ptr = new T[cap];
				v_si -= 1;
				for (int i = 0; i < v_si; i++)
					ptr[i] = temp[i];
				delete[] temp;
				for (int i = v_si; i < cap; i++)
					ptr[i] = NULL;
				return x;
		}
	}

	void insert(int ind, T const &index) // increase length by 1, put the value specified in the second argument at index specified by first argument and shift all values to the right in the vector down by one index
	{
		if (ind >= 0 && ind < v_si)
		{
			T *temp;
			temp = new T[v_si + 1];
			for (int i = 0; i < ind; i++)
			{
				temp[i] = ptr[i];
			}
			temp[ind] = index;
			for (int i = ind + 1; i < v_si + 1; i++)
			{
				temp[i] = ptr[i - 1];
			}
			v_si += 1;
			cap += 1;
			for (int i = 0; i < v_si; i++)
			{
				ptr[i] = temp[i];
			}
			delete[] temp;
		}
		else if (ind >= v_si)
		{
			cout<<"Inputed index cannot be out of range";
		}
		
	}

	void erase(int eindex) // decrease length by 1, remove the item at the specified index, and shift all other items left to eliminate the "hole"
	{
		if (eindex >= 0 && eindex < v_si)
		{
			T *temp;
			temp = new T[v_si - 1];
			for (int i = 0; i < eindex; i++)
			{
				temp[i] = ptr[i];
			}
			for (int i = eindex; i < v_si - 1; i++)
			{
				temp[i] = ptr[i + 1];
			}
			delete[] ptr;
			ptr = new T[v_si];
			for (int i = 0; i < v_si - 1; i++)
			{
				ptr[i] = temp[i];
			}
			ptr[v_si - 1] = NULL;
			delete[] temp;
		}
		else if (eindex >= v_si)
		{
			cout<<"Inputed index cannot be out of range";
		}
		
	}
	void erase(int i1, int i2) // same as erase(int), but removes all elements between the indexes specified by the first and second argument
	{
		if (i2 > i2 && i1 >= 0 && i2 >= 0 && i1 < v_si &&i2 < v_si)
		{
			int t = i2 - i1;
			T *temp;
			temp = new T[v_si];
			for (int i = 0; i < i1; i++)
			{
				temp[i] = ptr[i];
			}
			for (int i = i2; i < v_si - 1; i++)
			{
				temp[i - t] = ptr[i + 1];
			}
			delete[] ptr;
			ptr = new T[v_si];
			for (int i = 0; i < (v_si - t - 1); i++)
			{
				ptr[i] = temp[i];
			}
			for (int i = (v_si - t - 1); i < v_si; i++)
			{
				ptr[i] = NULL;
			}
			delete[] temp;
		}
		else if (i1 >= v_si || i2 >= v_si)
		{
			cout<<"Inputed index cannot be out of range";
		}
		
	}
	void clear() // remove all elements from the list
	{
		for (int i = 0; i < v_si; i++)
		{
			ptr[i] = NULL;
		}
	}
	T & operator[](int ind)
	{
		if ((v_si - 1) < ind)
		{
			cout<<"boundary surpassed";
		}
		else if (ind < v_si && ind >= 0)
		{
			return at(ind);
		}
		
	}
	T & operator[](int ind) const
	{
		if ((v_si - 1) < ind)
		{
			cout<<"boundary surpassed";
		}
		else if (ind < v_si && ind >= 0)
		{
			return at(ind);
		}
	}
	T& at(int ind)
	{
		if ((v_si - 1) < ind)
		{
			cout<<"boundary surpassed";
		}
		else if (ind <v_si && ind >= 0)
		{
			return ptr[ind];
		}
		else{
			cout<<"input can't be accpted";
		}
	}
	T& at(int ind) const
	{
		if ((v_si - 1) < ind)
		{
			cout<<"boundary surpassed";
		}
		else if (ind < v_si && ind >= 0)
		{
			return ptr[ind];
		}
	}
	~myvector()
	{
		if (ptr != nullptr)
			delete ptr;
	}
};
int main()
{	
		myvector<int> A;
		myvector<int> a(10);
		a[0] = 2;
		a[1] = 4;
		a[2] = 1;
		a[3] = 6;
		a[4] = 3;
		a[5] = 5;
		a[6] = 9;
		a[7] = 10;
		a[8] = 8;
		a[9] = 7;

		cout << "a :" << a.si() << "," << a.capa() << "\n";
		cout << "A :" << A.si() << "," << A.capa() << "\n";
	
		a.assign(3, 7);
		cout << a[3] << "\n";

		a.assign(2, 5);
		cout << a[2] << "\n";

		a.push_back(4);
		cout << a[10] << "," << a.si() << "\n";

		a.pop_back();
		cout << a.si() << "\n";
		
		myvector<int> x = A;
		x.resize(2);
		x[0] = 10;

		cout << x.si() << "," << x.capa() << "," << x[0]<<"\n";

		A.push_back(2);
		myvector<int> const y = A;
		int const z = y[0];
		cout << y.si() << "," << y[0] << "," << A[0];
}
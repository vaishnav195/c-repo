#include <iostream>
#include <vector> 

using namespace std;

class Tool
{
	/* Fill in */
protected:
	int strength;
	char type;
public:
	void setStrength(int st){
		strength = st;
	}
	int getstrength(){
		return strength;
	}
	char gettype(){
		return type;
	}
};
/*
Implement class Scissors
*/
class Scissors : public Tool
{
public:
		Scissors(int st)
	{
		strength = st;
		type = 's';
	}
		bool fight(Tool compare){
		switch (compare.gettype())
		{
		case 's':
			if (strength >= compare.getstrength())
				return true;
			else
				return false;
			break;
		case 'p':
			if ((strength * 2) >= compare.getstrength())
				return true;
			else
				return false;
			break;
		case 'r':
			if ((strength / 2) >= compare.getstrength())
				return true;
			else
				return false;
			break;
		default:
			break;
		}
	}
};
/*
Implement class Paper
*/
class Paper : public Tool
{
public:
	Paper(int st)
	{
		strength = st;
		type = 'p';
	}

	bool fight(Tool compare){
		switch (compare.gettype())
		{
		case 'p':
			if (strength >= compare.getstrength())
				return true;
			else
				return false;
			break;
		case 'r':
			if ((strength * 2) >= compare.getstrength())
				return true;
			else
				return false;
			break;
		case 's':
			if ((strength / 2) >= compare.getstrength())
				return true;
			else
				return false;
			break;
		default:
			break;
		}
	}
};
/*
Implement class Rock
*/
class Rock : public Tool
{
public:
	Rock(int st)
	{
		strength = st;
		type = 'r';
	}

	bool fight(Tool compare){
		switch (compare.gettype())
		{
		case 'r':
			if (strength >= compare.getstrength())
				return true;
			else
				return false;
			break;
		case 's':
			if ((strength * 2) >= compare.getstrength())
				return true;
			else
				return false;
			break;
		case 'p':
			if ((strength / 2) >= compare.getstrength())
				return true;
			else
				return false;
			break;
		default:
			break;
		}
	}
};
int main() {
	// Example main function
	// You may add your own testing code if you like
	Scissors s1(5);
	Paper p1(7);
	Rock r1(15);
	cout << s1.fight(p1) << p1.fight(s1) << endl;
	cout << p1.fight(r1) << r1.fight(p1) << endl;
	cout << r1.fight(s1) << s1.fight(r1) << endl;

	return 0;
}

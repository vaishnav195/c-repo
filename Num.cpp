#include<iostream>
#include<iomanip>

using namespace std;

int main()
{
	double x;//initializing a decimal variable
	int y;
	
	cout << "Input a Decimal number" << endl;
	cin >> x;//input a decimal number (user)
	
	cout << static_cast<int>(x) << endl;//casting from decimal to int without decimal points
	y = x;
	cout << y;
	return 0;
}

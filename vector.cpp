#include<iostream>
#include<vector>
using namespace std;

bool binarySearch(vector<int> &vect, int value)
{
	int first = 0;
	int last = vect.size() - 1;
	int mid;
	while (first <= last)
	{
		mid = (first + last) / 2;
		if (value < vect[mid])
			last = mid - 1;
		else if (value > vect[mid])
			first = mid + 1;
		else
			return true;
	}
	return false;
} 

int main()
{
	int tarVal;
	vector<int> vec{ 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	cout << "The " << vec.size() << " vector elements are : " << endl;
	for (int i = 0; i<vec.size(); i++)
		cout << vec[i] << " ";
	cout << endl << "enter target value :";
	cin >> tarVal;
	
	bool index = binarySearch(vec, tarVal);

	if (index)
		cout << "value found " << endl;

	else
		cout << "target value = " << tarVal << "  not found ";
	return 0;
}
